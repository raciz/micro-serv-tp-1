from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('student', views.student, name='student'),
    path('student/<int:studentId>', views.student_actions, name='student_actions'),
]