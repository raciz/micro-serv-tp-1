from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('lesson', views.lesson, name='lesson'),
    path('lesson/<int:lessonId>', views.lesson_actions, name='lesson_actions'),
]