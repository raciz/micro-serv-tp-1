from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import Subscribe, Notation

def index(request):
    return HttpResponse("Lesson subscribe Service Index")



def addSubscribe(data):
	print("#View Subscribe -> addSubscribe")
	try:
		subscribe = Subscribe.objects.create(student=int(data.get('student', None)),
										   lesson=int(data.get('lesson', None)))
		response = {"state": True, 'data': subscribe.to_json()}
	except Exception as e:
		print("ERROR: " + str(e))
		response = {"state": False, 'error': str(e)}
	return response


def getAllSubscribes():
	print("#View Subscribe -> GetAllSubscribes")
	try:
		response = list()
		subscribes = Subscribe.objects.all()
		for subscribe in subscribes:
			response.append(subscribe.to_json())
		response = {"state": True, "data": response}
	except Exception as e:
		print("ERROR: " + str(e))
		response = {"state": False, 'error': str(e)}
	return response


def deleteStudent(id):
	print("#View Subscribe -> deleteSubscribe")
	try:
		subscribe = Subscribe.objects.get(id=id).delete()
		response = {"state": True, 'idDelete':id}
	except Exception as e:
		response = {"state": False, 'error': str(e)}
	return response


@csrf_exempt
def subscribe(request):
	if request.method == 'POST':
		response = addSubscribe(request.POST)
	elif request.method == 'GET':
		response = getAllSubscribes()
	else:
		return HttpResponseNotFound('<h1>Page not found</h1>')
	return JsonResponse(response)


@csrf_exempt
def subscribe_actions(request, subscribeId):
	if request.method == 'DELETE':
		response = deleteStudent(subscribeId)
	else:
		return HttpResponseNotFound('<h1>Page not found</h1>')
	return JsonResponse(response)


@csrf_exempt
def add_notation(request):
	if (request.method != 'POST'):
		return HttpResponseNotFound('<h1>Page not found</h1>')
	try:
		notation = Notation.objects.create(subscribe=int(request.POST.get("subscribe", None)),
										   note=int(request.POST.get("note", None)))
		response = {"state": True, 'data': notation.to_json()}
	except Exception as e:
		print("ERROR: " + str(e))
		response = {"state": False, 'error': str(e)}
	return JsonResponse(response)


@csrf_exempt
def get_coverage(request, lessonId):
	if (request.method != 'POST'):
		return HttpResponseNotFound('<h1>Page not found</h1>')
	try:
		allNotes = Notation.objects.filter()
		response = {"state": True, 'data': notation.to_json()}
	except Exception as e:
		print("ERROR: " + str(e))
		response = {"state": False, 'error': str(e)}
	return JsonResponse(response)




