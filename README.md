# micro-serv-tp-1

FIRST ACTIVE VIRTUALENV

source venv/bin/activate

INIT DATABASE

    -create in utf8:
        -> micro-serv-lesson
        -> micro-serv-student
        -> micro-serv-lesson_subscribe
    
    -set your log in 
        -> lesson/lesson/settings.py
        -> student/student/settings.py
        -> lesson_subscribe/lesson_subscribe/settings.py
        
FOR EACH SERVICES

    in project folder:
        -> python manage.py makemigrations service
        -> python manage.py migrate
        -> python manage.py runserver 0.0.0.0:8000 (port voulu)


---> url in project/project/urls.py and project/service/urls.py
    
        